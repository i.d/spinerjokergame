package com.joker.spin.game.app.ui.game

data class NumberItem(val id: Int){
    companion object {
        val numbers =
            arrayOf(3, 14, 7, 16, 20, 1 , 8, 2, 9, 15, 19, 5, 17, 6, 11, 18, 4, 10, 13, 12)
    }

    val number = numbers[id]

    val color = getColor(id)

    private fun getColor(id: Int): GameViewModel.Color {
        return if(id % 2 == 0) GameViewModel.Color.RED
        else GameViewModel.Color.BLACK
    }
}
