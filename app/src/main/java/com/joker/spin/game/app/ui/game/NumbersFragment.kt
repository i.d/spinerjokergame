package com.joker.spin.game.app.ui.game

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.joker.spin.game.app.R
import com.joker.spin.game.app.databinding.PzxankjvjaxwjtieBinding
import com.joker.spin.game.app.ui.adapters.NumberAdapter
import com.joker.spin.game.app.ui.game.NumberItem.Companion.numbers
import kotlin.math.min

class NumbersFragment : DialogFragment(R.layout.pzxankjvjaxwjtie){
    private val viewModel: GameViewModel by activityViewModels()
    private lateinit var binding: PzxankjvjaxwjtieBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = PzxankjvjaxwjtieBinding.bind(view)
        binding.vsnaqbxvhciksbra.apply {
            adapter = NumberAdapter(List(numbers.size){NumberItem(it)}){
                viewModel.guessedNumber.value = it
                dismiss()
            }
            layoutManager = LinearLayoutManager(requireContext())
        }
        val height = requireContext().resources.displayMetrics.heightPixels
        requireDialog().window?.setLayout(convertDipToPixels(200f),
            min(convertDipToPixels( 350f),height - 100))
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    fun convertDipToPixels(dips: Float): Int {
        return (dips * requireContext().resources.displayMetrics.density + 0.5f).toInt()
    }
}