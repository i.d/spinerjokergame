package com.joker.spin.game.app.ui.web

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.joker.spin.game.app.R
import com.joker.spin.game.app.databinding.VrfdjsexhrtwryrlBinding
import com.joker.spin.game.app.ui.loading.FireFetch
import org.imaginativeworld.oopsnointernet.dialogs.signal.NoInternetDialogSignal

class WebFragment : Fragment(R.layout.vrfdjsexhrtwryrl) {
    private lateinit var binding: VrfdjsexhrtwryrlBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = VrfdjsexhrtwryrlBinding.bind(view)
        with(binding) {
            WebBuilder.adapt(this@WebFragment,ickuxghmghgcmxhc,bsxwbhvrsrgpsmfg,ynfhcbedbmhgxfdg)
            ickuxghmghgcmxhc.loadUrl(FireFetch.result!!)
        }
        NoInternetDialogSignal.Builder(
            requireActivity(),
            lifecycle
        ).apply {
            dialogProperties.apply {
                cancelable = false
                showAirplaneModeOffButtons = false
                showInternetOnButtons = false
                noInternetConnectionTitle = "Нету интернета"
                noInternetConnectionMessage = "Включите интернет"
            }
        }.build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        WebBuilder.onActivityResult(resultCode, data)
    }
}