package com.joker.spin.game.app.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.joker.spin.game.app.databinding.XxrnrwfnuiyafmgqBinding
import com.joker.spin.game.app.ui.game.GameViewModel
import com.joker.spin.game.app.ui.game.NumberItem

class NumberAdapter(private val items: List<NumberItem>, private val listener: (NumberItem) -> Unit) : RecyclerView.Adapter<NumberAdapter.NumberViewHolder>(){
    class NumberViewHolder(val binding: XxrnrwfnuiyafmgqBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberViewHolder {
        return NumberViewHolder(
            XxrnrwfnuiyafmgqBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
        val item = items[position]
        holder.binding.apply {
            tnjwyomupplhcrow.text = item.number.toString()
            root.setBackgroundColor(
                if(item.color == GameViewModel.Color.RED) Color.RED
                else Color.BLACK
            )
            root.setOnClickListener {
                listener(item)
            }

        }
    }

    override fun getItemCount() = items.size
}