package com.joker.spin.game.app.application

import com.joker.spin.game.app.BuildConfig
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

object YandexMetricaCreator {
    fun create(application: ZXPUj25U4i4muyUB){
        YandexMetrica.activate(
            application,
            YandexMetricaConfig.newConfigBuilder(BuildConfig.YANDEX_METRICA_KEY).build()
        )
        YandexMetrica.enableActivityAutoTracking(application)
    }
}