package com.joker.spin.game.app.ui.loading

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.joker.spin.game.app.R
import com.joker.spin.game.app.databinding.LmdyesthpufqjftxBinding
import com.joker.spin.game.app.ui.game.MainGameFragment
import com.joker.spin.game.app.ui.web.WebFragment
import java.util.*

class LoadingFragment : Fragment(R.layout.lmdyesthpufqjftx) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FireFetch.fetch(view.findViewById(R.id.tempWeb))
        Timer().schedule(task,5000L)
    }
    private val task = object : TimerTask(){
        override fun run() {
            requireActivity().supportFragmentManager.commit {
                if (FireFetch.result == null || !FireFetch.isDone) {
                    replace(R.id.nowzcmamcogfgfqk, MainGameFragment())
                } else {
                    replace(R.id.nowzcmamcogfgfqk, WebFragment())
                }
            }
        }
    }

}