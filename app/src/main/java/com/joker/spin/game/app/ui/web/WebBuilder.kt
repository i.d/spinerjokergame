package com.joker.spin.game.app.ui.web

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.webkit.*
import android.widget.ProgressBar
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.joker.spin.game.app.R
import com.kinnerapriyap.sugar.Shergil
import com.kinnerapriyap.sugar.choice.MimeType

object WebBuilder {
    fun adapt(
        fragment: WebFragment,
        web: WebView,
        refreshLayout: SwipeRefreshLayout,
        progressBar: ProgressBar
    ) {
        addSettings(web)
        addCookies(web)
        addChromeClient(fragment, web, progressBar)
        addViewClient(web, refreshLayout, progressBar)
        addOnRefresh(web, refreshLayout)
        addOnBackPressed(fragment, web)
    }

    private fun addOnBackPressed(fragment: WebFragment, web: WebView) {
        fragment.requireActivity().onBackPressedDispatcher.addCallback {
            if(web.canGoBack())web.goBack()
            else {
                isEnabled = false
                fragment.requireActivity().onBackPressed()
            }
        }
    }

    private fun addOnRefresh(web: WebView, refreshLayout: SwipeRefreshLayout) {
        refreshLayout.setOnRefreshListener {
            web.reload()
        }
    }

    fun onActivityResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            fpc?.onReceiveValue(Shergil.getMediaUris(data).toTypedArray())
        } else {
            fpc?.onReceiveValue(null)
        }
    }

    private fun addChromeClient(fragment: WebFragment, web: WebView, progressBar: ProgressBar) {
        web.webChromeClient = getChrome(fragment, progressBar)
    }

    private var fpc: ValueCallback<Array<Uri>>? = null
    private fun getChrome(fragment: WebFragment, progressBar: ProgressBar) =
        object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                progressBar.progress = newProgress
            }

            override fun onShowFileChooser(
                webView: WebView?,
                filePathCallback: ValueCallback<Array<Uri>>?,
                fileChooserParams: FileChooserParams?
            ): Boolean {
                fpc = filePathCallback
                Shergil.create(fragment)
                    .mimeTypes(MimeType.IMAGES)
                    .showDisallowedMimeTypes(false)
                    .theme(R.style.Shergil)
                    .maxSelectable(1)
                    .allowCamera(true)
                    .withRequestCode(1080)
                return true
            }
        }

    private fun addViewClient(
        web: WebView,
        refreshLayout: SwipeRefreshLayout,
        progressBar: ProgressBar
    ) {
        web.webViewClient = getView(refreshLayout, progressBar)
    }

    private fun getView(refreshLayout: SwipeRefreshLayout, progressBar: ProgressBar) =
        object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBar.isVisible = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.isVisible = false
                refreshLayout.isRefreshing = false
            }
        }

    private fun addCookies(web: WebView) {
        CookieManager.setAcceptFileSchemeCookies(true)
        CookieManager.getInstance()
            .setAcceptThirdPartyCookies(web, true)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun addSettings(web: WebView) = with(web.settings) {
        displayZoomControls = false
        allowFileAccess = true
        useWideViewPort = true
        defaultTextEncodingName = "utf-8"
        javaScriptEnabled = true
        builtInZoomControls = true
        cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        allowContentAccess = true
        domStorageEnabled = true
        loadWithOverviewMode = true
    }


}