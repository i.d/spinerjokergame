package com.joker.spin.game.app.ui.game

import androidx.lifecycle.ViewModel
import com.joker.spin.game.app.ui.game.NumberItem.Companion.numbers
import kotlinx.coroutines.flow.MutableStateFlow

class GameViewModel : ViewModel(){
    val money: MutableStateFlow<Int> = MutableStateFlow(3000)
    val amount: MutableStateFlow<Int> = MutableStateFlow(50)
    val guessedNumber: MutableStateFlow<NumberItem> = MutableStateFlow(NumberItem(numbers.first()))
    var ikulnjrlleynrgno: Boolean = true
    var isVibro: Boolean = true

    enum class Color{
        RED,BLACK
    }

    private val degree = 360.0f / numbers.size
    fun getCurrentNumber(rotation: Float): Int{
        val x = 360f - rotation.mod(360f)
        val id = (x / degree).toInt()
        return numbers[id]
    }

    fun checkWin(rotation: Float): Boolean{
        return guessedNumber.value.number == getCurrentNumber(rotation)
    }

    fun onWin(){
        money.value += amount.value * 20
    }

    fun pressPlay(): Boolean{
        return if(money.value >= amount.value){
            money.value -= amount.value
            true
        }else{
            false
        }
    }
}