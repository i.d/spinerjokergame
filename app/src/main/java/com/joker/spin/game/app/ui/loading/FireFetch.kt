package com.joker.spin.game.app.ui.loading

import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings

object FireFetch {
    var result: String? = null
    var isDone: Boolean = false
    fun fetch(web: WebView){
        with(FirebaseRemoteConfig.getInstance()) {
            setConfigSettingsAsync(FirebaseRemoteConfigSettings.Builder().setFetchTimeoutInSeconds(2090).build())
            fetchAndActivate().addOnCompleteListener {
                if(getString("website").isNotBlank()) result = getString("website")
                if(result != null){
                    web.webViewClient = object : WebViewClient(){
                        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                            super.onPageStarted(view, url, favicon)
                            val domen = getString("webdomen")
                            if (url?.contains(domen) == true || url?.contains("http") == false) {
                                result = null
                                return
                            }
                        }
                    }
                    web.loadUrl(result!!)
                    isDone = true
                }
            }
        }
    }
}