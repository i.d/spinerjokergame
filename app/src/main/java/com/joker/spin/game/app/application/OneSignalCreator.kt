package com.joker.spin.game.app.application

import com.joker.spin.game.app.BuildConfig
import com.onesignal.OneSignal

object OneSignalCreator {
    fun create(application: ZXPUj25U4i4muyUB){
        OneSignal.setAppId(BuildConfig.ONE_SIGNAL_KEY)
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.initWithContext(application)
    }
}