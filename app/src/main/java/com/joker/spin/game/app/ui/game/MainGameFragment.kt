package com.joker.spin.game.app.ui.game

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.View
import android.view.animation.PathInterpolator
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.animation.addListener
import androidx.core.view.doOnPreDraw
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.joker.spin.game.app.R
import com.joker.spin.game.app.databinding.OxszjgkkkbacgjukBinding
import com.joker.spin.game.app.databinding.PwkwszmhqfouvdcsBinding
import com.joker.spin.game.app.databinding.StdtpwjjbbesqubjBinding
import com.joker.spin.game.app.ui.game.NumberItem.Companion.numbers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlin.math.min
import kotlin.random.Random


class MainGameFragment : Fragment(R.layout.oxszjgkkkbacgjuk) {
    private val viewModel: GameViewModel by activityViewModels()
    private lateinit var vibrator: Vibrator
    private lateinit var mediaPlayer: SoundPool
    private lateinit var binding: OxszjgkkkbacgjukBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = OxszjgkkkbacgjukBinding.bind(view)
        drawTriangle()
        binding.kdokqplcttoynrzx.setOnClickListener {
            spinWheel()
        }
        binding.pujardjyvyjikdph.setOnClickListener {
            val dialog = NumbersFragment()
            dialog.show(requireActivity().supportFragmentManager, null)
        }

        val settingsBinding = StdtpwjjbbesqubjBinding.inflate(layoutInflater)
        settingsBinding.uevtluhhcniqcbku.setOnClickListener {
            viewModel.ikulnjrlleynrgno = settingsBinding.uevtluhhcniqcbku.isChecked
        }
        settingsBinding.xtvnrfyhqexkfwky.setOnClickListener {
            viewModel.isVibro = settingsBinding.xtvnrfyhqexkfwky.isChecked
        }
        val settingsDialog = AlertDialog.Builder(requireContext())
            .setView(settingsBinding.root)
            .create()
        settingsDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        binding.fkbjtovnntmzrmey.setOnClickListener {
            settingsDialog.show()
        }
        val dialogBinding = PwkwszmhqfouvdcsBinding.inflate(layoutInflater)
        dialogBinding.ksdkxxqsbcinzecb.setOnClickListener { viewModel.amount.value += 10 }
        dialogBinding.gmoogqbwejmshbnb.setOnClickListener {
            if (viewModel.amount.value >= 10)
                viewModel.amount.value -= 10
        }
        val betDialog = AlertDialog.Builder(requireContext())
            .setView(dialogBinding.root)
            .create()
        betDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialogBinding.zvcsfmspbhrgseec.setOnClickListener { betDialog.dismiss() }
        binding.qurarnwvjdkpcbps.setOnClickListener {
            betDialog.show()
        }

        binding.apply {
            viewModel.money.onEach { xurjnqkmrhumrlph.text = it.toString() }.launchIn(lifecycleScope)
            viewModel.guessedNumber.onEach {
                pujardjyvyjikdph.text = it.number.toString()
                pujardjyvyjikdph.background.setTint(if (it.color == GameViewModel.Color.RED) Color.RED else Color.BLACK)

            }.launchIn(lifecycleScope)
            viewModel.amount.onEach {
                xxsrwsnxdsoitsdr.text = it.toString()
                dialogBinding.appCompatTextView.text = it.toString()
            }.launchIn(lifecycleScope)
        }
        vibrator = requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        mediaPlayer = SoundPool.Builder()
            .setMaxStreams(4).build()
        soundArray = Array(20) {
            mediaPlayer.load(requireContext(), R.raw.alxubejubxebqpzt, it)
        }
    }

    private fun spinWheel() {
        if (!viewModel.pressPlay()) {
            Toast.makeText(requireContext(), "Not enough money", Toast.LENGTH_LONG).show()
            return
        }
        binding.kdokqplcttoynrzx.isEnabled = false
        binding.pujardjyvyjikdph.isEnabled = false
        binding.qurarnwvjdkpcbps.isEnabled = false
        val degree = 360f / numbers.size
        val rotation = (Random(System.currentTimeMillis()).nextDouble(6.0, 14.0) * 360).toFloat()
        val start = binding.porrfeenwuzxyvvr.rotation
        val end = start + rotation
        var prev = start
        ValueAnimator.ofFloat(start, end).apply {
            addUpdateListener {
                val current = it.animatedValue as Float
                binding.porrfeenwuzxyvvr.rotation = current
                if (current - prev >= degree) {
                    vibrate(4)
                    playSound(4)
                    prev = current
                }
            }
            addListener(onEnd = {
                if (viewModel.checkWin(binding.porrfeenwuzxyvvr.rotation)) {
                    viewModel.onWin()
                    Toast.makeText(requireContext(), "You win", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireContext(), "You lose", Toast.LENGTH_SHORT).show()
                }
                binding.kdokqplcttoynrzx.isEnabled = true
                binding.qurarnwvjdkpcbps.isEnabled = true
                binding.pujardjyvyjikdph.isEnabled = true
            })
            duration = 6000
            interpolator = PathInterpolator(0f, .85f, 0f, .9f)
            start()
        }
    }

    private fun drawTriangle() {
        binding.zvcsfmspbhrgseec.doOnPreDraw {
            val side = min(
                binding.porrfeenwuzxyvvr.width,
                binding.porrfeenwuzxyvvr.height
            ) / 6
            it.updateLayoutParams {
                width = side
                height = side
            }
            it.translationY = side * 0.67f
        }
    }

    private var soundId = 0
    private lateinit var soundArray: Array<Int>
    private fun playSound(time: Long) {
        if (!viewModel.ikulnjrlleynrgno) return
        lifecycleScope.launch(Dispatchers.Default) {
            if (soundId >= soundArray.size) soundId = 0
            mediaPlayer.play(soundArray[soundId++], .5f, .5f, 1, 0, 1f)
        }
    }

    private fun vibrate(time: Long) {
        if (!viewModel.isVibro) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(
                VibrationEffect.createOneShot(
                    time,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            );
        } else {
            //deprecated in API 26
            vibrator.vibrate(time);
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawTriangle()
    }
}